<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert(
[
	[
            'title' => 'Task1',
            'user_id' => '1',
            'created_at' => date('Y-m-d G:i:s'),
	],
	[
            'title' => 'Task2',
            'user_id' => '1',
            'created_at' => date('Y-m-d G:i:s'),
	],
	[
            'title' => 'Task3',
            'user_id' => '1',
            'created_at' => date('Y-m-d G:i:s'),
	],

        ]);
    }
}
