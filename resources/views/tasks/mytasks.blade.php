<h1>My Tasks</h1>
<a href="{{route('tasks.index')}}">Show all Tasks</a>
<ul>
    @foreach($tasks as $task)
    <li>
             id: {{$task->id}} title:{{$task->title}} <a href="{{route('tasks.edit',$task->id )}}">Edit Task </a>
             @cannot('user') <a href="{{route('delete',$task->id)}}">Delete Task </a>@endcannot 
          
        @if($task->status == 1)
        <a> Task is Done</a>
        @else
        @cannot('user')  <a input type='url' href="{{route('update',$task->id)}}">Mark As Done </a>@endif @endcannot
         </li>
         @endforeach

         <a href="{{route('tasks.create')}}">Create New Task </a>